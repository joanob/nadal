export default class Regalo {
  constructor(id, gifterId, propietaryId, pic) {
    this.id = id;
    this.gifterId = gifterId;
    this.propietaryId = propietaryId;
    this.pic = pic;
  }

  static addRegalo(regalos, setRegalos, content) {
    let req = new XMLHttpRequest();
    req.open("GET", "http://localhost:8080/img/" + content.pic);
    req.onreadystatechange = () => {
      if (req.readyState === XMLHttpRequest.DONE) {
        if (req.status === 200) {
          let extension = content.pic.split(".");
          regalos.push(
            new Regalo(
              content.id,
              content.gifterId,
              content.propietaryId,
              "data:image/" +
                extension[extension.length - 1] +
                ";base64, " +
                req.responseText
            )
          );
          setRegalos(regalos);
        } else {
          /* ERROR */
        }
      }
    };
    req.send();
  }
}
