import React, { useRef, useContext } from "react";
import { WS } from "./Game";

export default function Login(props) {
  const nameRef = useRef();
  const codeRef = useRef();
  const [ws, login] = useContext(WS);

  const submitHandler = (e) => {
    e.preventDefault();
    login(nameRef.current.value, codeRef.current.value);
  };

  return (
    <div className="modalContainer">
      <div className="login">
        <h1>Inicia sessió</h1>
        <form onSubmit={submitHandler}>
          <label htmlFor="name">Nom</label>
          <input type="text" id="name" ref={nameRef} />
          <label htmlFor="code">Codi</label>
          <input type="text" id="code" ref={codeRef} />
          <input type="submit" value="Jugar" />
        </form>
      </div>
    </div>
  );
}
