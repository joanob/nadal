import React, { useContext } from "react";
import { Game } from "./Game";

export default function Play() {
  const [game, setGame] = useContext(Game);
  return (
    <div>
      {game.regalos.map((regalo, i) => {
        return <img src={regalo.pic} key={i}></img>;
      })}
    </div>
  );
}
