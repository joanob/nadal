import React, { useContext } from "react";
import ReactDOM from "react-dom";

// Context
import GameContext, { Game } from "./Game";
import { WSContext } from "./Game";

// Components
import Login from "./Login";
import Upload from "./Upload";
import Play from "./Play";

const App = () => {
  const [game, setGame] = useContext(Game);
  return !game.userId ? <Login /> : !game.imgUploaded ? <Upload /> : <Play />;
};

ReactDOM.render(
  <WSContext>
    <GameContext>
      <App />
    </GameContext>
  </WSContext>,
  document.getElementById("root")
);
