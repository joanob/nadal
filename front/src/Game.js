import React, { useState, useEffect, useContext } from "react";
import Regalo from "./objects/Regalo";

export const Game = React.createContext();
export const WS = React.createContext();

export const WSContext = (props) => {
  const [ws, setWs] = useState(false);

  useEffect(() => {
    if (ws) {
      ws.onopen = () => {
        let userId = localStorage.getItem("userId");
        let name = localStorage.getItem("name");
        ws.send(
          JSON.stringify({ userId: userId, action: "LOGIN", content: name })
        );
      };
      ws.onerror = (e) => {
        console.log("WS Error: " + e);
      };
      ws.onclose = () => {
        console.log("WS closed");
        setWs(null);
      };
    }
  }, [ws]);

  const login = (name, code) => {
    localStorage.setItem("name", name);
    localStorage.setItem("userId", code);
    setWs(new WebSocket("ws://localhost:8080"));
  };

  return <WS.Provider value={[ws, login]}>{props.children}</WS.Provider>;
};

const GameContext = (props) => {
  const [ws, login] = useContext(WS);
  const [state, setState] = useState({
    userId: false,
    userName: false,
    imgUploaded: false,
    regalos: [],
  });
  let notListening = true;

  const editRegalos = (regalos) => {
    setState((state) => ({ ...state, regalos: regalos }));
  };

  const inMessage = (e) => {
    const msg = JSON.parse(e.data);
    switch (msg.action) {
      case "ADDREGALO":
        Regalo.addRegalo(state.regalos, editRegalos, msg.content);
        break;
      default:
        break;
    }
  };

  if (ws) {
    if (notListening) {
      ws.onmessage = inMessage;
      notListening = false;
    }
    if (state.userId === false) {
      let userId = localStorage.getItem("userId");
      let name = localStorage.getItem("name");
      if (name && userId) {
        setState((state) => ({ ...state, userId: userId, userName: name }));
      }
    }
  }

  const uploadImgs = (img1, img2) => {
    let req = new XMLHttpRequest();
    req.open("POST", "http://localhost:8080/fotos");
    req.setRequestHeader("Authorization", state.userId);
    let formData = new FormData();
    formData.append("userId", state.userId);
    formData.append("img1", img1);
    formData.append("img2", img2);
    req.onreadystatechange = () => {
      if (req.readyState === XMLHttpRequest.DONE) {
        if (req.status === 200) {
          setState((state) => ({ ...state, imgUploaded: true }));
        }
      }
    };
    req.send(formData);
  };

  const functions = { uploadImgs };

  return (
    <Game.Provider value={[state, functions]}>{props.children}</Game.Provider>
  );
};

export default GameContext;
