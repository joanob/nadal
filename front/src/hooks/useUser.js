import { useContext } from "react";
import { Game } from "./Game";

const useUser = () => {
  const [game, setGame] = useContext(Game);

  const setUserId = (userId) => {
    setGame((game) => ({ ...game, userId: userId }));
  };

  return { setUserId: setUserId };
};

export default useUser;
