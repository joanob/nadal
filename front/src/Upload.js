import React, { useState, useContext } from "react";
import { Game } from "./Game";

export default function Upload() {
  const [img1, setImg1] = useState(false);
  const [img2, setImg2] = useState(false);
  const [game, setGame] = useContext(Game);

  const imgChange = (e) => {
    if (e.target.files && e.target.files[0]) {
      if (e.target.files[0].type.split("/")[0] !== "image") {
        alert("No es una imatge");
        return;
      }
      let reader = new FileReader();
      reader.onload = (ev) => {
        if (e.target.id === "uploadImg1") {
          setImg1(ev.target.result);
        } else {
          setImg2(ev.target.result);
        }
      };
      reader.readAsDataURL(e.target.files[0]);
    }
  };

  const upload = () => {
    const i1 = document.getElementById("uploadImg1").files[0];
    const i2 = document.getElementById("uploadImg2").files[0];
    if (i1 && i2) {
      setGame.uploadImgs(i1, i2);
    } else {
      alert("Selecciona les dos fotos");
    }
  };

  return (
    <div className="modalContainer">
      <div className="upload">
        <h1>Volem vore els regals!</h1>
        <form
          className="uploadGrid"
          method="POST"
          encType="multipart/form-data"
        >
          <div
            className="uploadedImg"
            style={img1 ? { backgroundImage: "url(" + img1 + ")" } : {}}
          ></div>
          <div
            className="uploadedImg"
            style={img1 ? { backgroundImage: "url(" + img2 + ")" } : {}}
          ></div>
          <div className="imgSelector">
            <input type="file" id="uploadImg1" onChange={imgChange} />
          </div>
          <div className="imgSelector">
            <input type="file" id="uploadImg2" onChange={imgChange} />
          </div>
        </form>
        <button className="uploadButton" onClick={upload}>
          Pujar
        </button>
      </div>
    </div>
  );
}
