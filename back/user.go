package main

import (
	"encoding/json"

	"github.com/gorilla/websocket"
)

type User struct {
	Name string
	Id   string
	Conn *websocket.Conn
}

func login(msg InMessage, ws *websocket.Conn) {
	for i, user := range users {
		if user.Id == msg.SenderId {
			// Update ws
			users[i].Conn = ws
			return
		}
	}
	// User doesn't exist in active users. Add user
	for _, code := range codes {
		if code == msg.SenderId {
			user := User{Name: string(msg.Content), Id: code, Conn: ws}
			users = append(users, user)
			return
		}
	}
}

func broadcast(msg OutMessage) {
	p, _ := json.Marshal(msg)
	for _, user := range users {
		user.Conn.WriteMessage(1, p)
	}
}
