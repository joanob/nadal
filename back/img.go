package main

import (
	"bufio"
	"encoding/base64"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"mime/multipart"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

/* FALTA AGREGAR REGALS AL ARRAY DE REGALS I TORNAR-LOS A TOTS ELS USUARIS */

func handleGetImg(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	w.Header().Set("Access-Control-Allow-Headers", "Origin, Accept, Content-Type, Content-Length, Authorization, X-Auth-Token")
	if r.Method != http.MethodGet {
		w.WriteHeader(200)
		return
	}
	reqPath := strings.Split(r.URL.Path, "/")
	img, err := os.Open("/Users/joan/go/src/gitlab.com/joanob/regaloback/img/" + reqPath[len(reqPath)-1])
	if err != nil {
		log.Fatal(err)
	}
	defer img.Close()
	reader := bufio.NewReader(img)
	content, _ := ioutil.ReadAll(reader)
	contentType := http.DetectContentType(content)
	w.Header().Set("Content-Type", contentType)
	w.Write([]byte(base64.StdEncoding.EncodeToString(content)))
}

func handleImgUpdate(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	w.Header().Set("Access-Control-Allow-Headers", "Origin, Accept, Content-Type, Content-Length, Authorization, X-Auth-Token")
	if r.Method != http.MethodPost {
		w.WriteHeader(200)
		return
	}

	userId := r.Header.Get("Authorization")
	isValidUser := false
	for _, user := range users {
		if user.Id == userId {
			isValidUser = true
		}
	}
	if !isValidUser {
		w.WriteHeader(403)
		return
	}

	img1, handle, err := r.FormFile("img1")
	if err != nil {
		fmt.Println(err)
		w.WriteHeader(400)
		return
	}
	defer img1.Close()
	ok1 := saveFile(userId, img1, handle)

	img1, handle, err = r.FormFile("img2")
	if err != nil {
		fmt.Println(err)
		w.WriteHeader(400)
		return
	}
	defer img1.Close()
	ok2 := saveFile(userId, img1, handle)

	if ok1 && ok2 {
		w.WriteHeader(200)
	}
}

func saveFile(userId string, file multipart.File, handle *multipart.FileHeader) bool {
	data, err := ioutil.ReadAll(file)
	if err != nil {
		fmt.Println(err)
		return false
	}

	filename := strconv.FormatInt(time.Now().UnixNano(), 10) + "." + strings.Split(handle.Header.Get("Content-Type"), "/")[1]

	err = ioutil.WriteFile("/Users/joan/go/src/gitlab.com/joanob/regaloback/img/"+filename, data, 0777)
	if err != nil {
		fmt.Println(err)
		return false
	}
	addRegalo(userId, filename)
	return true
}

func createRand() string {
	var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
	b := make([]rune, 5)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}
