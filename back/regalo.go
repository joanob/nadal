package main

import "encoding/json"

// PropietaryId -1 taula
type Regalo struct {
	Id           int    `json:"regaloId"`
	GifterId     string `json:"gifterId"`
	PropietaryId string `json:"propietaryId"`
	Pic          string `json:"pic"`
}

var regalos []Regalo

func addRegalo(gifterId string, pic string) {
	regalo := Regalo{Id: len(regalos), GifterId: gifterId, PropietaryId: "-1", Pic: pic}
	regalos = append(regalos, regalo)

	// Broadcast regalo
	p, _ := json.Marshal(regalo)
	broadcast(OutMessage{Action: "ADDREGALO", Content: p})
}
