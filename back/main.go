package main

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gorilla/websocket"
)

type InMessage struct {
	SenderId string          `json:"userId"`
	Action   string          `json:"action"`
	Content  json.RawMessage `json:"content"`
}

type OutMessage struct {
	Action  string          `json:"action"`
	Content json.RawMessage `json:"content"`
}

var users []User

var codes []string

var upgrader = websocket.Upgrader{ReadBufferSize: 1024, WriteBufferSize: 1024, CheckOrigin: func(r *http.Request) bool { return true }}

func start(w http.ResponseWriter, r *http.Request) {
	// Crea la conexió
	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		fmt.Println(err)
	}

	go func(ws *websocket.Conn) {
		for {
			_, p, err := ws.ReadMessage()
			if err != nil {
				fmt.Println(err)
			}

			msg := InMessage{}

			if json.Unmarshal(p, &msg) != nil {
				return
			}

			switch msg.Action {
			case "LOGIN":
				login(msg, ws)
			case "":
				// Message
			}
		}
	}(ws)

}

func main() {
	codes = []string{"a", "b", "c"}

	http.HandleFunc("/", start)

	http.HandleFunc("/fotos", handleImgUpdate)

	http.HandleFunc("/img/", handleGetImg)

	http.ListenAndServe(":8080", nil)
}
